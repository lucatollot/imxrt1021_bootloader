/***************************************************************************//**
* @file IMXRT1021_boot_qsp_management.h
*
* @brief Hyperflash Service Routines.
*
* @details Constants, structures, enumerations and functions definition for 
*          "IMXRT1021_boot_qsp_management.c" file.
*
*
* @todo None
*
*
* @author Luca Tollot @date {08/01/2020}
******************************************************************************/

#ifndef __QSP_MNG__
#define __QSP_MNG__


/* Includes ------------------------------------------------------------------*/
#include "board.h"
#include "fsl_flexspi.h"
#include "fsl_cache.h"



//Definitions

#define QSP_FLASH_SIZE 0x2000 // 64 Mb / 8192 KByte 
#define QSP_FLEXSPI_AMBA_BASE FlexSPI_AMBA_BASE
#define QSP_FLASH_PAGE_SIZE 256 // 256 byte
#define QSP_SECTOR_SIZE 0x1000 // 4 KByte 
#define QSP_NUM_SECTOR_PAGE (QSP_SECTOR_SIZE / QSP_FLASH_PAGE_SIZE)
#define QSP_NUM_SECTOR ((QSP_FLASH_SIZE * 1000) / SECTOR_SIZE)
#define QSP_FLEXSPI_CLOCK kCLOCK_FlexSpi

#define NOR_CMD_LUT_SEQ_IDX_READ_NORMAL 7
#define NOR_CMD_LUT_SEQ_IDX_READ_FAST 13
#define NOR_CMD_LUT_SEQ_IDX_READ_FAST_QUAD 0
#define NOR_CMD_LUT_SEQ_IDX_READSTATUS 1
#define NOR_CMD_LUT_SEQ_IDX_WRITEENABLE 2
#define NOR_CMD_LUT_SEQ_IDX_ERASESECTOR 3
#define NOR_CMD_LUT_SEQ_IDX_PAGEPROGRAM_SINGLE 6
#define NOR_CMD_LUT_SEQ_IDX_PAGEPROGRAM_QUAD 4
#define NOR_CMD_LUT_SEQ_IDX_READID 8
#define NOR_CMD_LUT_SEQ_IDX_WRITESTATUSREG 9
#define NOR_CMD_LUT_SEQ_IDX_ENTERQPI 10
#define NOR_CMD_LUT_SEQ_IDX_EXITQPI 11
#define NOR_CMD_LUT_SEQ_IDX_READSTATUSREG 12
#define NOR_CMD_LUT_SEQ_IDX_ERASECHIP 5

#define CUSTOM_LUT_LENGTH 60
#define FLASH_QUAD_ENABLE 0x40
#define FLASH_BUSY_STATUS_POL 1
#define FLASH_BUSY_STATUS_OFFSET 0
#define FLASH_ERROR_STATUS_MASK 0x0E





#define QSP_MAX_NUMBER_IMAGES 10
#define QSP_DATA_SIZE_ENTRY_TABLE 4
#define QSP_ADDRESS_SIZE_ENTRY_TABLE 4
#define QSP_IMG_SIZE_ENTRY_TABLE 4
#define QSP_SECTOR_START_ENTRY_TABLE 2
#define QSP_SECTOR_SIZE_ENTRY_TABLE 2
#define QSP_IMG_CRC_ENTRY_TABLE 4
#define QSP_SERIAL_OPTIONS_SIZE 8
#define QSP_CRC_SIZE_ENTRY_TABLE 2
#define QSP_ELEMENTS_ENTRY_TABLE (QSP_ADDRESS_SIZE_ENTRY_TABLE + QSP_IMG_SIZE_ENTRY_TABLE + QSP_SECTOR_START_ENTRY_TABLE + QSP_SECTOR_SIZE_ENTRY_TABLE + QSP_IMG_CRC_ENTRY_TABLE)
#define QSP_SIZE_ENTRY_TABLE_BUFFER (QSP_DATA_SIZE_ENTRY_TABLE + QSP_MAX_NUMBER_IMAGES * QSP_ELEMENTS_ENTRY_TABLE + QSP_SERIAL_OPTIONS_SIZE + QSP_CRC_SIZE_ENTRY_TABLE)


#define QSP_ENTRY_TABLE_SECTOR 11 //Entry table position
#define QSP_ENTRY_TABLE_ADDR (QSP_ENTRY_TABLE_SECTOR * QSP_SECTOR_SIZE)
#define QSP_OFFSET_IMAGE_VECTOR_TABLE 0x1000
#define QSP_OFFSET_VECTOR_TABLE 0x2000
#define QSP_SIZE_INTERRUPT 256



//static const uint8_t pattern[] = {0x72, 0xB6, 0x05, 0x48, 0x05, 0x49, 0x01, 0x60, 0x0A, 0x68, 0x82, 0xF3, 0x08, 0x88, 0x04, 0x48, 0x80, 0x47, 0x62, 0xB6, 0x03, 0x48, 0x00, 0x47, 0x08, 0xED, 0x00, 0xE0};



static const uint32_t customLUT[CUSTOM_LUT_LENGTH] = 
{
    /* Normal read mode -SDR */
    /* Normal read mode -SDR */
    [4 * NOR_CMD_LUT_SEQ_IDX_READ_NORMAL] =
        FLEXSPI_LUT_SEQ(kFLEXSPI_Command_SDR, kFLEXSPI_1PAD, 0x03, kFLEXSPI_Command_RADDR_SDR, kFLEXSPI_1PAD, 0x18),
    [4 * NOR_CMD_LUT_SEQ_IDX_READ_NORMAL + 1] =
        FLEXSPI_LUT_SEQ(kFLEXSPI_Command_READ_SDR, kFLEXSPI_1PAD, 0x04, kFLEXSPI_Command_STOP, kFLEXSPI_1PAD, 0),

    /* Fast read mode - SDR */
    [4 * NOR_CMD_LUT_SEQ_IDX_READ_FAST] =
        FLEXSPI_LUT_SEQ(kFLEXSPI_Command_SDR, kFLEXSPI_1PAD, 0x0B, kFLEXSPI_Command_RADDR_SDR, kFLEXSPI_1PAD, 0x18),
    [4 * NOR_CMD_LUT_SEQ_IDX_READ_FAST + 1] = FLEXSPI_LUT_SEQ(
        kFLEXSPI_Command_DUMMY_SDR, kFLEXSPI_1PAD, 0x08, kFLEXSPI_Command_READ_SDR, kFLEXSPI_1PAD, 0x04),

    /* Fast read quad mode - SDR */
    [4 * NOR_CMD_LUT_SEQ_IDX_READ_FAST_QUAD] =
        FLEXSPI_LUT_SEQ(kFLEXSPI_Command_SDR, kFLEXSPI_1PAD, 0xEB, kFLEXSPI_Command_RADDR_SDR, kFLEXSPI_4PAD, 0x18),
    [4 * NOR_CMD_LUT_SEQ_IDX_READ_FAST_QUAD + 1] = FLEXSPI_LUT_SEQ(
        kFLEXSPI_Command_DUMMY_SDR, kFLEXSPI_4PAD, 0x06, kFLEXSPI_Command_READ_SDR, kFLEXSPI_4PAD, 0x04),

    /* Read extend parameters */
    [4 * NOR_CMD_LUT_SEQ_IDX_READSTATUS] =
        FLEXSPI_LUT_SEQ(kFLEXSPI_Command_SDR, kFLEXSPI_1PAD, 0x81, kFLEXSPI_Command_READ_SDR, kFLEXSPI_1PAD, 0x04),

    /* Write Enable */
    [4 * NOR_CMD_LUT_SEQ_IDX_WRITEENABLE] =
        FLEXSPI_LUT_SEQ(kFLEXSPI_Command_SDR, kFLEXSPI_1PAD, 0x06, kFLEXSPI_Command_STOP, kFLEXSPI_1PAD, 0),

    /* Erase Sector  */
    [4 * NOR_CMD_LUT_SEQ_IDX_ERASESECTOR] =
        FLEXSPI_LUT_SEQ(kFLEXSPI_Command_SDR, kFLEXSPI_1PAD, 0xD7, kFLEXSPI_Command_RADDR_SDR, kFLEXSPI_1PAD, 0x18),

    /* Page Program - single mode */
    [4 * NOR_CMD_LUT_SEQ_IDX_PAGEPROGRAM_SINGLE] =
        FLEXSPI_LUT_SEQ(kFLEXSPI_Command_SDR, kFLEXSPI_1PAD, 0x02, kFLEXSPI_Command_RADDR_SDR, kFLEXSPI_1PAD, 0x18),
    [4 * NOR_CMD_LUT_SEQ_IDX_PAGEPROGRAM_SINGLE + 1] =
        FLEXSPI_LUT_SEQ(kFLEXSPI_Command_WRITE_SDR, kFLEXSPI_1PAD, 0x04, kFLEXSPI_Command_STOP, kFLEXSPI_1PAD, 0),

    /* Page Program - quad mode */
    [4 * NOR_CMD_LUT_SEQ_IDX_PAGEPROGRAM_QUAD] =
        FLEXSPI_LUT_SEQ(kFLEXSPI_Command_SDR, kFLEXSPI_1PAD, 0x32, kFLEXSPI_Command_RADDR_SDR, kFLEXSPI_1PAD, 0x18),
    [4 * NOR_CMD_LUT_SEQ_IDX_PAGEPROGRAM_QUAD + 1] =
        FLEXSPI_LUT_SEQ(kFLEXSPI_Command_WRITE_SDR, kFLEXSPI_4PAD, 0x04, kFLEXSPI_Command_STOP, kFLEXSPI_1PAD, 0),

    /* Read ID */
    [4 * NOR_CMD_LUT_SEQ_IDX_READID] =
        FLEXSPI_LUT_SEQ(kFLEXSPI_Command_SDR, kFLEXSPI_1PAD, 0x9F, kFLEXSPI_Command_READ_SDR, kFLEXSPI_1PAD, 0x04),

    /* Enable Quad mode */
    [4 * NOR_CMD_LUT_SEQ_IDX_WRITESTATUSREG] =
        FLEXSPI_LUT_SEQ(kFLEXSPI_Command_SDR, kFLEXSPI_1PAD, 0x01, kFLEXSPI_Command_WRITE_SDR, kFLEXSPI_1PAD, 0x04),

    /* Enter QPI mode */
    [4 * NOR_CMD_LUT_SEQ_IDX_ENTERQPI] =
        FLEXSPI_LUT_SEQ(kFLEXSPI_Command_SDR, kFLEXSPI_1PAD, 0x35, kFLEXSPI_Command_STOP, kFLEXSPI_1PAD, 0),

    /* Exit QPI mode */
    [4 * NOR_CMD_LUT_SEQ_IDX_EXITQPI] =
        FLEXSPI_LUT_SEQ(kFLEXSPI_Command_SDR, kFLEXSPI_4PAD, 0xF5, kFLEXSPI_Command_STOP, kFLEXSPI_1PAD, 0),

    /* Read status register */
    [4 * NOR_CMD_LUT_SEQ_IDX_READSTATUSREG] =
        FLEXSPI_LUT_SEQ(kFLEXSPI_Command_SDR, kFLEXSPI_1PAD, 0x05, kFLEXSPI_Command_READ_SDR, kFLEXSPI_1PAD, 0x04),

    /* Erase whole chip */
    [4 * NOR_CMD_LUT_SEQ_IDX_ERASECHIP] =
        FLEXSPI_LUT_SEQ(kFLEXSPI_Command_SDR, kFLEXSPI_1PAD, 0xC7, kFLEXSPI_Command_STOP, kFLEXSPI_1PAD, 0),
};




//Structures

typedef struct 
{
  uint16_t addr;                                          //!< address
  uint8_t padd1;                                          //!< padding
  uint8_t padd2;                                          //!< padding
  uint32_t baudRate;                                      //!< baud rate
}qspSerialOptions_t;

typedef struct 
{
  uint32_t addr;                                          //!< Image address
  uint32_t size;                                          //!< Image size
  uint16_t startSector;                                   //!< Start sector
  uint16_t numSector;                                     //!< Number sectors
  uint32_t crc;                                           //!< Image crc
}qspInfoImage_t;

typedef struct 
{
  uint8_t numberImages;                                   //!< Number images
  uint8_t selImage;                                       //!< Image selected
  uint16_t timeOutBootLife;                               //!< time out boot life
  qspInfoImage_t infoImage[QSP_MAX_NUMBER_IMAGES];        //!< Info Image
  qspSerialOptions_t serialOptions;                       //!< Serial options
  uint16_t crc;                                           //!< Crc image                    
}qspEntryTableStruct_t;

typedef union 
{
  uint8_t buffer[QSP_SIZE_ENTRY_TABLE_BUFFER];            //!< Buffer 
  qspEntryTableStruct_t entryTableStruct;                 //!< Struct entry table
}qspEntryTable_t;

typedef struct 
{
  uint32_t addr;                                          //!< Image address
  uint32_t size;                                          //!< Image size
  uint16_t startSector;                                   //!< Start sector
  uint16_t numSector;                                     //!< Number sectors
  uint8_t nImage;                                         //!< Image number
  uint32_t crc;                                           //!< Crc image                 
}qspNewImg_t;

/*typedef struct
{
  uint32_t hdr;                                           //!< hdr with tag #HAB_TAG_IVT, length and HAB version fields
  uint32_t entry;                                         //!< Absolute address of the first instruction to execute from the image
  uint32_t reserved1;                                     //!< Reserved in this version of HAB: should be NULL. 
  uint32_t dcd;                                           //!< Absolute address of the image DCD: may be NULL.
  uint32_t boot_data;                                     //!< Absolute address of the Boot Data: may be NULL, but not interpreted any further by HAB
  uint32_t self;                                          //!< Absolute address of the IVT.
  uint32_t csf;                                           //!< Absolute address of the image CSF.
  uint32_t reserved2;                                     //!< Reserved in this version of HAB: should be zero. 
  uint8_t buf[QSP_FLASH_PAGE_SIZE - 32];
}qspIvt_t;*/

/*typedef union
{
  qspIvt_t structure;                                     //!< Structure
  uint8_t buffer[sizeof(qspIvt_t)];                       //!< Buffer structure
}qspImageVectorTable_t;*/

/*typedef union
{
  uint8_t buffer[QSP_SECTOR_SIZE];                        //!< Buffer byte
  uint32_t buffer32[QSP_SECTOR_SIZE / sizeof(uint32_t)];  //!< Buffer qword
}qspSectorBuffer_t;*/

typedef struct 
{
  qspNewImg_t newImg;                                     //!< Data new image received
  uint8_t buffer[QSP_SECTOR_SIZE];                        //!< Buffer   
  uint16_t part;                                          //!< General purpose word
  //uint16_t nsectorOld;                                    //!< Store previous sector
  qspEntryTable_t entryTable;                             //!< Entry table   
  //uint32_t tempImgSize;                                   //!< Temporary image size
  //uint32_t addPageTmp;                                    //!< Temporary address page
  //uint8_t checkImg[QSP_MAX_NUMBER_IMAGES];                //!< Check delete images
  //qspImageVectorTable_t imageVectorTable;                 //!< Image vector table
  //uint32_t vectorTable[QSP_SIZE_INTERRUPT / 4];           //!< Vector table
  //qspSectorBuffer_t sectorBuffer;                         //!< Sector buffer      
}qspControlStatus_t;


//Functions

void QSP_flexspi_nor_flash_init();
status_t QSP_flexspi_nor_flash_erase_sector(uint32_t nsector);
status_t QSP_flexspi_nor_flash_page_program(uint32_t dstAddr, uint32_t *src);
status_t QSP_flexspi_nor_flash_program(uint32_t dstAddr, uint32_t *src, size_t size);
status_t QSP_flexspi_nor_get_vendor_id(uint8_t *vendorId);
status_t QSP_flexspi_nor_enable_quad_mode();
status_t QSP_flexspi_nor_erase_chip();
status_t QSP_Sector_Program(uint32_t nsector, uint32_t *src);





#endif