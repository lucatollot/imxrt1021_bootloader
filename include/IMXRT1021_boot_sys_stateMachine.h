/***************************************************************************//**
* @file IMXRT1021_boot_sys_stateMachine.h
*
* @brief Sys Service Routines.
*
* @details Constants, structures, enumerations and functions definition for 
*          "IMXRT1021_boot_sys_stateMachine.c" file.
*
*
* @todo None
*
*
* @author Luca Tollot @date {29/12/2019}
******************************************************************************/

#ifndef __SYS_STATE_MACHINE__
#define __SYS_STATE_MACHINE__


/* Includes ------------------------------------------------------------------*/


//Constants

#define SYS_TIME_OUT_MACHINE 5000                                       // Timeout value state machine (ms)
#define TIME_OUT_BOOT_LIFE_DEFAULT 6000                                 // Timeout boot life (ms)


//Enumetations

typedef enum  
{
  SYS_INIT_STATE = 0,                                                   //!< Init state
  SYS_WAIT_STATE = 1,                                                   //!< Wait state
  SYS_PING_STATE = 2,                                                   //!< Ping state
  SYS_REBOOT_STATE = 3,                                                 //!< Reboot state
  SYS_STRIMG_STATE = 4,                                                 //!< Strimg state
  SYS_UPLOAD_STATE = 5,                                                 //!< Upload state
  SYS_DOWNLOAD_STATE = 6,                                               //!< Download state
  SYS_RDENTAB_STATE = 7,                                                //!< Read entry table state
  SYS_WRENTAB_STATE = 8,                                                //!< Write entry table state
  SYS_SETIMAGE_STATE = 9,                                               //!< Set image state
  SYS_SETBOOTIME_STATE = 10,                                            //!< Set life boot time state
  SYS_RDINFO_STATE = 11,                                                //!< Read info
  SYS_DELIMG_STATE = 12,                                                //!< Delete image
  SYS_ERROR_STATE = 13,                                                 //!< Error state
  SYS_RESET_STATE = 14                                                  //!< Reset state
}sysState;



//Structures

typedef struct 
{
  uint16_t rel;                                                         //!< Release
  uint16_t subrel;                                                      //!< Subrelease
  uint16_t fam;                                                         //!< Family
  uint16_t day;                                                         //!< Day
  uint16_t month;                                                       //!< Month
  uint16_t year;                                                        //!< Year
}
sysInfo_t;



typedef struct 
{
  volatile sysState state;                                              //!< State of machine
  volatile uint16_t timeOutMachine;                                     //!< Timeout state machine
  uint16_t timeOutBootLife;                                             //!< Timeout boot life
  bool enableTimeOutBootLife;                                           //!< Enable timeout boot life
  uint8_t phase;                                                        //!< Phase
  uint8_t errCode;                                                      //!< Error code
  sysInfo_t info;                                                       //!< Info
  uint8_t *flashPointer;                                                //!< Flash pointer
  bool enableReset;                                                     //!< Enable reset
  bool endUploadPck;                                                    //!< End receive packet
}sysStateMachine_t;



//Functions

int16_t SYS_Init();
void SYS_StateMachine();
bool SYS_Read_Entry_Table();
void SYS_Store_Entry_Table();





#endif