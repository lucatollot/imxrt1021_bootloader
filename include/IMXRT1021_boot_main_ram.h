/***************************************************************************//**
* @file IMXRT1021_boot_main_ram.c
*
* @brief Main Service Routines.
*
* @details This file handles main.
*
*
* @todo None
*
*
* @author Luca Tollot @date {29/12/2019}
******************************************************************************/


#ifndef __MAIN__
#define __MAIN__


/* Includes ------------------------------------------------------------------*/

#include "board.h"

#include "IMXRT1021_boot_uty_management.h"
#include "IMXRT1021_boot_tim_management.h"
#include "IMXRT1021_boot_qsp_management.h"

#include "IMXRT1021_boot_sys_stateMachine.h"
#include "IMXRT1021_boot_urt_stateMachine.h"





//Versione e revisione

#define RELEASE 01
#define SUBRELEASE 01
#define DATA_DAY 27
#define DATA_MONTH 01
#define DATA_YEAR 20
#define FAMILY 100






//Costanti

#define WATCHDOG_TIME 2000    // watch dog time refresh (ms)




extern timControlStatus_t timControlStatusObj; 
//extern utyControlStatus_t utyControlStatusObj;
extern qspControlStatus_t qspControlStatusObj;

extern sysStateMachine_t sysStateMachineObj;
extern urtStateMachine_t urtStateMachineObj[2];


extern uint32_t entryPoint;

void Reset_Handler();
void BOARD_InitModuleClock();
void RTWDOG_InitModule();
//void LPI2C_UserCallback(LPI2C_Type *base, lpi2c_master_edma_handle_t *handle, status_t completionStatus, void *userData);
void LPUART_UserCallback(LPUART_Type *base, lpuart_edma_handle_t *handle, status_t status, void *userData);





#endif