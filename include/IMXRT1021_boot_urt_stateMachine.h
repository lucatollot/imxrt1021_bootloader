/***************************************************************************//**
* @file IMXRT1021_boot_urt_stateMachine.h
*
* @brief Uart Service Routines.
*
* @details Constants, structures, enumerations and functions definition for 
*          "IMXRT1021_boot_urt_stateMachine.c" file.
*
*
* @todo None
*
*
* @author Luca Tollot @date {03/01/2020}
******************************************************************************/

#ifndef __URT_STATE_MACHINE__
#define __URT_STATE_MACHINE__


/* Includes ------------------------------------------------------------------*/
#include "fsl_lpuart.h"
#include "fsl_lpuart_edma.h"
#include "fsl_dmamux.h"



//Definitions

#define URT_LPUART_CLK_FREQ BOARD_DebugConsoleSrcFreq()

#define URT_LPUART_DMAMUX_BASEADDR DMAMUX
#define URT_LPUART_DMA_BASEADDR DMA0


#define URT_LPUART1 LPUART1
#define URT_LPUART1_IRQn LPUART1_IRQn
#define URT_LPUART1_IRQHandler LPUART1_IRQHandler
#define URT_LPUART1_TX_DMA_CHANNEL 0U
#define URT_LPUART1_RX_DMA_CHANNEL 1U
#define URT_LPUART1_TX_DMA_REQUEST kDmaRequestMuxLPUART1Tx
#define URT_LPUART1_RX_DMA_REQUEST kDmaRequestMuxLPUART1Rx

static const uint32_t URT_DMA_CONFIG_PORT_1[] = {URT_LPUART1_TX_DMA_CHANNEL, URT_LPUART1_RX_DMA_CHANNEL, URT_LPUART1_TX_DMA_REQUEST, URT_LPUART1_RX_DMA_REQUEST};

#define URT_LPUART2 LPUART6
#define URT_LPUART2_IRQn LPUART6_IRQn
#define URT_LPUART2_IRQHandler LPUART6_IRQHandler
#define URT_LPUART2_TX_DMA_CHANNEL 2U
#define URT_LPUART2_RX_DMA_CHANNEL 3U
#define URT_LPUART2_TX_DMA_REQUEST kDmaRequestMuxLPUART6Tx
#define URT_LPUART2_RX_DMA_REQUEST kDmaRequestMuxLPUART6Rx

static const uint32_t URT_DMA_CONFIG_PORT_2[] = {URT_LPUART2_TX_DMA_CHANNEL, URT_LPUART2_RX_DMA_CHANNEL, URT_LPUART2_TX_DMA_REQUEST, URT_LPUART2_RX_DMA_REQUEST};






#define URT_TIME_OUT_MACHINE 5000                                        // Timeout value state machine (ms)
#define URT_DEFAULT_ADDR 1                                               // Default address
//#define URT_DEFAULT_BAUD_RATE 38400                                      // Default baud rate
#define URT_DEFAULT_BAUD_RATE 115200                                     // Default baud rate
#define URT_TIMEOUT_RX 5                                                 // Timeout rx


#define URT_SIZE_BUFFER 256                                              // Size buffer

#define URT_MAX_NUM_REG 100                                              // Max registers number

#define URT_TIMEOUT_UPLOAD 20000                                         // Valore timeout durante upload

#define URT_MAX_SIZE_MISS_PCK 8192                                       // Max size to allocate missing packets

//#define URT_SIZE_CODE_PCK 128                                            // Size code for packet



//ASK RECEPTION MSG
//Sub Request structure
#define URT_MSG_ASK_FILE_LENGTH 3
#define URT_MSG_ASK_REF_TYPE (URT_MSG_ASK_FILE_LENGTH + 1)
#define URT_MSG_ASK_MISS_REC (URT_MSG_ASK_REF_TYPE + 1)

#define URT_MSG_ASK_SIZE 42




//Enumetations

typedef enum  
{
  URT_INIT_STATE = 0,                                                   //!< Init state
  URT_STANDBY_STATE = 1,                                                //!< Standby state
  URT_MODBUS_STATE = 2,                                                 //!< Modbus state
  URT_ERROR_STATE = 10,                                                 //!< Error state
  URT_RESET_STATE = 11                                                  //!< Reset state
}urtState;


//Structures

typedef union
{
  uint8_t value;
  struct
  {
    uint8_t ENDRX             : 1;    //!< bit end rx
    uint8_t ENDTX             : 1;    //!< bit end tx   
    uint8_t ONGOING           : 1;    //!< bit ongoing    
  }; 
}stateUart_t;

typedef struct 
{
  IRQn_Type interruptID;                                                //!< Interrupt ID
  LPUART_Type *port;                                                    //!< Port number
  uint32_t baudRate;                                                    //!< LPUART baud rate  
  lpuart_parity_mode_t parityMode;                                      //!< Parity mode, disabled (default), even, odd 
  lpuart_data_bits_t dataBitsCount;                                     //!< Data bits count, eight (default), seven 
  lpuart_stop_bit_count_t stopBitCount;                                 //!< Number of stop bits, 1 stop bit (default) or 2 stop bits 
  const uint32_t *configDMA;                                            //!< Pointer DMA configuration             
}urtSettings_t;


typedef struct 
{
  uint8_t value;                                                        //!< Check block
}urtInfoPacket_t;

typedef struct 
{
  uint16_t numPckToRecv;                                                //!< Number of packet to receive
  uint16_t cntPck;                                                      //!< Counter pck
  uint16_t numFile;                                                     //!< File number
  urtInfoPacket_t infoPacket[URT_MAX_SIZE_MISS_PCK];                    //!< Info packet received
  uint16_t timeout;                                                     //!< Timeout
  uint8_t recoveryMode;                                                 //!< Recovery mode
}urtDataPacket_t;


typedef struct 
{
  volatile urtState state;                                              //!< State of machine
  volatile uint16_t timeOutMachine;                                     //!< Timeout state machine
  uint8_t bufferRx[URT_SIZE_BUFFER];                                    //!< Packet received
  uint8_t bufferTx[URT_SIZE_BUFFER];                                    //!< Packet trasmitted
  volatile uint16_t timeOutRx;                                          //!< Time out receive pck
  uint16_t cntRx;                                                       //!< Char counter receiver
  uint16_t cntTx;                                                       //!< Char counter trasmitter
  uint16_t nByteToSend;                                                 //!< Number byte to send
  volatile stateUart_t stateUart;                                       //!< State uart
  uint8_t address;                                                      //!< Modbus address
  urtDataPacket_t dataPacket;                                           //!< Data packet
  urtSettings_t settings;                                               //!< Settings uart
  lpuart_config_t config;                                               //!< Uart configuration
  lpuart_edma_handle_t g_lpuartEdmaHandle;
  edma_handle_t g_lpuartTxEdmaHandle;
  edma_handle_t g_lpuartRxEdmaHandle;
  edma_config_t edmaconfig;
  lpuart_transfer_t sendXfer;
  lpuart_transfer_t receiveXfer;
}urtStateMachine_t;





//Functions

int16_t URT_Init(urtStateMachine_t *urtStateMachine);
int16_t URT_DeInit(urtStateMachine_t *urtStateMachine);
void URT_Enable_RX(bool enable, urtStateMachine_t *urtStateMachine);
void URT_Enable_TX(bool enable, urtStateMachine_t *urtStateMachine);
void URT_StateMachine(urtStateMachine_t *urtStateMachine);
void URT_Decode_Msg(urtStateMachine_t *urtStateMachine);
void URT_Reset(urtStateMachine_t *urtStateMachine);
void URT_Send_Exception(urtStateMachine_t *urtStateMachine);
uint8_t URT_Function_03(urtStateMachine_t *urtStateMachine);
uint8_t URT_Function_06(urtStateMachine_t *urtStateMachine);
uint8_t URT_Function_16(urtStateMachine_t *urtStateMachine);
uint8_t URT_Function_20(urtStateMachine_t *urtStateMachine);
uint8_t URT_Function_21(urtStateMachine_t *urtStateMachine);
void URT_IRQHandler(urtStateMachine_t *urtStateMachine);
void URT_LPUART_UserCallback(LPUART_Type *base, lpuart_edma_handle_t *handle, status_t status, void *userData);








#endif