/***************************************************************************//**
* @file IMXRT1021_boot_main_flash.h
*
* @brief Main Service Routines.
*
* @details This file handles main.
*
*
* @todo None
*
*
* @author Luca Tollot @date {29/12/2019}
******************************************************************************/


#ifndef __MAIN__
#define __MAIN__


/* Includes ------------------------------------------------------------------*/

#include "fsl_common.h"

#include "IMXRT1021_boot_uty_management.h"

//Costanti

#define STARTADDR 0x00000000 //Start memory address
#define SIZE 32000 //Size bin file
#define ADDR_BOOT_FLASH 0x60003000 //address flash boot






#endif