/***************************************************************************//**
* @file IMXRT1021_boot_tim_management.h
*
* @brief Timer Service Routines.
*
* @details Constants, structures, enumerations and functions definition for 
*          "IMXRT1021_boot_tim_management.c" file.
*
*
* @todo None
*
*
* @author Luca Tollot @date {29/12/2019}
******************************************************************************/

#ifndef __TIM_MNG__
#define __TIM_MNG__


/* Includes ------------------------------------------------------------------*/
#include "fsl_pit.h"

//Definitions

#define TIM_PIT_IRQHandler PIT_IRQHandler
#define PIT_IRQ_ID PIT_IRQn
#define PIT_SOURCE_CLOCK CLOCK_GetFreq(kCLOCK_OscClk) // Get source clock for PIT driver 
#define TIM_CLOCK_PERIOD 1000U //1 ms


#define LED_INIT() USER_LED_INIT(LOGIC_LED_OFF)
#define LED_TOGGLE() USER_LED_TOGGLE()



//Structures

typedef union
{
  struct
  {
    uint8_t V0 : 1;                                            
    uint8_t V1 : 1;      
    uint8_t V2 : 1;      
    uint8_t V3 : 1;      
    uint8_t V4 : 1;      
    uint8_t V5 : 1;      
    uint8_t V6 : 1;      
    uint8_t V7 : 1;
  };
  uint8_t value;
}timFlags1_t;

typedef struct 
{
  uint8_t millCnt;                                      //!< Milliseconds counter
  uint8_t dec;                                          //!< Decimal part of second
  uint8_t sec;                                          //!< Seconds
  uint8_t min;                                          //!< Minutes
  uint8_t hours;                                        //!< Hours        
}timMng_t;

typedef struct 
{
  volatile timMng_t timMng;                             //!< Clock reference
  pit_config_t pitConfig;                               //!< Structure of initialize PIT
}timControlStatus_t;


//Functions

int16_t TIM_Init_Class();
int16_t TIM_TimeModel();
void TIM_PIT_IRQHandler(void);





#endif