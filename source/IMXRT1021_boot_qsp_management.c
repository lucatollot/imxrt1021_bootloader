/***************************************************************************//**
* @file IMXRT1021_boot_qsp_management.c
*
* @brief Hyperflash Service Routines.
*
* @details This file handles qspi flash peripherals.
*
*
* @todo None
*
*
* @author Luca Tollot @date {08/01/2020}
******************************************************************************/

/* Includes ------------------------------------------------------------------*/
#include "IMXRT1021_boot_main_ram.h"





flexspi_device_config_t deviceconfig = 
{
    .flexspiRootClk       = 30000000,
    .flashSize            = QSP_FLASH_SIZE,
    .CSIntervalUnit       = kFLEXSPI_CsIntervalUnit1SckCycle,
    .CSInterval           = 2,
    .CSHoldTime           = 3,
    .CSSetupTime          = 3,
    .dataValidTime        = 0,
    .columnspace          = 0,
    .enableWordAddress    = 0,
    .AWRSeqIndex          = 0,
    .AWRSeqNumber         = 0,
    .ARDSeqIndex          = NOR_CMD_LUT_SEQ_IDX_READ_FAST_QUAD,
    .ARDSeqNumber         = 1,
    .AHBWriteWaitUnit     = kFLEXSPI_AhbWriteWaitUnit2AhbCycle,
    .AHBWriteWaitInterval = 0,
};


/***************************************************************************//**
* FuncName: {QSP_flexspi_nor_flash_init}
*
* @brief This function initializes qsp object.
*
* @details This function initializes qsp object and returns state
*          of initialization.
*
* @todo None
*
* IN:
* @param[in] None
*            
*
* OUT:
* @param[out] None
*
* RET:
* @retval None
*
*
* @author {Luca Tollot} @date {08/01/2020}
******************************************************************************/
   
void QSP_flexspi_nor_flash_init()
{
  flexspi_config_t config;
  status_t status;

  //flexspi_clock_init();
  
  #if defined(XIP_EXTERNAL_FLASH) && (XIP_EXTERNAL_FLASH == 1)
    // Switch to PLL2 for XIP to avoid hardfault during re-initialize clock. 
    CLOCK_InitSysPfd(kCLOCK_Pfd2, 24);    // Set PLL2 PFD2 clock 396MHZ. 
    CLOCK_SetMux(kCLOCK_FlexspiMux, 0x2); // Choose PLL2 PFD2 clock as flexspi source clock.
    CLOCK_SetDiv(kCLOCK_FlexspiDiv, 2);   // flexspi clock 133M. 
  #else
    const clock_usb_pll_config_t g_ccmConfigUsbPll = {.loopDivider = 0U};

    CLOCK_InitUsb1Pll(&g_ccmConfigUsbPll);
    CLOCK_InitUsb1Pfd(kCLOCK_Pfd0, 24);   // Set PLL3 PFD0 clock 360MHZ. 
    CLOCK_SetMux(kCLOCK_FlexspiMux, 0x3); // Choose PLL3 PFD0 clock as flexspi source clock. 
    CLOCK_SetDiv(kCLOCK_FlexspiDiv, 2);   // flexspi clock 120M. 
  #endif 

  // Get FLEXSPI default settings and configure the flexspi. 
  FLEXSPI_GetDefaultConfig(&config);

  // Set AHB buffer size for reading data through AHB bus. 
  config.ahbConfig.enableAHBPrefetch    = true;
  config.ahbConfig.enableAHBBufferable  = true;
  config.ahbConfig.enableReadAddressOpt = true;
  config.ahbConfig.enableAHBCachable    = true;
  config.rxSampleClock                  = kFLEXSPI_ReadSampleClkLoopbackFromDqsPad;
  FLEXSPI_Init(FLEXSPI, &config);

  // Configure flash settings according to serial flash feature. 
  FLEXSPI_SetFlashConfig(FLEXSPI, &deviceconfig, kFLEXSPI_PortA1);

  // Update LUT table. 
  FLEXSPI_UpdateLUT(FLEXSPI, 0, customLUT, CUSTOM_LUT_LENGTH);

  // Do software reset. 
  FLEXSPI_SoftwareReset(FLEXSPI);
  
  memset ((void*)qspControlStatusObj.buffer, 0xFF, sizeof(qspControlStatusObj.buffer));
  
  status = QSP_flexspi_nor_enable_quad_mode(); // Enter quad mode
  
  if(status != kStatus_Success)
  {}
}

/***************************************************************************//**
* FuncName: {QSP_flexspi_nor_write_enable}
*
* @brief This function enable write.
*
* @details This function enable write.
*
* @todo None
*
* IN:
* @param[in] uint32_t baseAddr: mandatory, base memory address
*            
*
* OUT:
* @param[out] None
*
* RET:
* @retval status_t status
*
*
* @author {Luca Tollot} @date {08/01/2020}
******************************************************************************/

status_t QSP_flexspi_nor_write_enable(uint32_t baseAddr)
{
  flexspi_transfer_t flashXfer;
  status_t status;

  // Write enable 
  flashXfer.deviceAddress = baseAddr;
  flashXfer.port          = kFLEXSPI_PortA1;
  flashXfer.cmdType       = kFLEXSPI_Command;
  flashXfer.SeqNumber     = 1;
  flashXfer.seqIndex      = NOR_CMD_LUT_SEQ_IDX_WRITEENABLE;

  status = FLEXSPI_TransferBlocking(FLEXSPI, &flashXfer);

  return status;
}

/***************************************************************************//**
* FuncName: {QSP_flexspi_nor_wait_bus_busy}
*
* @brief This function wait bus until it is busy.
*
* @details This function wait bus until it is busy.
*
* @todo None
*
* IN:
* @param[in] None
*            
*
* OUT:
* @param[out] None
*
* RET:
* @retval status_t status
*
*
* @author {Luca Tollot} @date {08/01/2020}
******************************************************************************/

status_t QSP_flexspi_nor_wait_bus_busy()
{
  // Wait status ready. 
  bool isBusy;
  uint32_t readValue;
  status_t status;
  flexspi_transfer_t flashXfer;

  flashXfer.deviceAddress = 0;
  flashXfer.port          = kFLEXSPI_PortA1;
  flashXfer.cmdType       = kFLEXSPI_Read;
  flashXfer.SeqNumber     = 1;
  flashXfer.seqIndex      = NOR_CMD_LUT_SEQ_IDX_READSTATUSREG;
  flashXfer.data          = &readValue;
  flashXfer.dataSize      = 1;

  do
  {
    status = FLEXSPI_TransferBlocking(FLEXSPI, &flashXfer);

    if(status != kStatus_Success)
    {
      return status;
    }
    if(FLASH_BUSY_STATUS_POL)
    {
      if(readValue & (1U << FLASH_BUSY_STATUS_OFFSET))
      {
        isBusy = true;
      }
      else
      {
        isBusy = false;
      }
    }
    else
    {
      if(readValue & (1U << FLASH_BUSY_STATUS_OFFSET))
      {
        isBusy = false;
      }
      else
      {
        isBusy = true;
      }
    }

  }while(isBusy);

  return status;
}

/***************************************************************************//**
* FuncName: {QSP_flexspi_nor_enable_quad_mode}
*
* @brief This function enable quad mode.
*
* @details This function enable quad mode.
*
* @todo None
*
* IN:
* @param[in] None
*            
*
* OUT:
* @param[out] None
*
* RET:
* @retval status_t status
*
*
* @author {Luca Tollot} @date {08/01/2020}
******************************************************************************/

status_t QSP_flexspi_nor_enable_quad_mode()
{
  flexspi_transfer_t flashXfer;
  status_t status;
  uint32_t writeValue = FLASH_QUAD_ENABLE;

  // Write enable 
  status = QSP_flexspi_nor_write_enable(0);

  if(status != kStatus_Success)
  {
    return status;
  }

  // Enable quad mode. 
  flashXfer.deviceAddress = 0;
  flashXfer.port          = kFLEXSPI_PortA1;
  flashXfer.cmdType       = kFLEXSPI_Write;
  flashXfer.SeqNumber     = 1;
  flashXfer.seqIndex      = NOR_CMD_LUT_SEQ_IDX_WRITESTATUSREG;
  flashXfer.data          = &writeValue;
  flashXfer.dataSize      = 1;

  status = FLEXSPI_TransferBlocking(FLEXSPI, &flashXfer);
  
  if(status != kStatus_Success)
  {
    return status;
  }

  status = QSP_flexspi_nor_wait_bus_busy();

  // Do software reset. 
  FLEXSPI_SoftwareReset(FLEXSPI);

  return status;
}

/***************************************************************************//**
* FuncName: {QSP_flexspi_nor_flash_erase_sector}
*
* @brief This function erase a sector.
*
* @details This function erase a sector.
*
* @todo None
*
* IN:
* @param[in] uint32_t nsector: mandatory, sector number
*            
*
* OUT:
* @param[out] None
*
* RET:
* @retval status_t status
*
*
* @author {Luca Tollot} @date {08/01/2020}
******************************************************************************/

status_t QSP_flexspi_nor_flash_erase_sector(uint32_t nsector)
{
  status_t status;
  flexspi_transfer_t flashXfer;
  uint32_t address = nsector * QSP_SECTOR_SIZE;

  // Write enable 
  flashXfer.deviceAddress = address;
  flashXfer.port          = kFLEXSPI_PortA1;
  flashXfer.cmdType       = kFLEXSPI_Command;
  flashXfer.SeqNumber     = 1;
  flashXfer.seqIndex      = NOR_CMD_LUT_SEQ_IDX_WRITEENABLE;

  status = FLEXSPI_TransferBlocking(FLEXSPI, &flashXfer);

  if(status != kStatus_Success)
  {
    return status;
  }

  flashXfer.deviceAddress = address;
  flashXfer.port          = kFLEXSPI_PortA1;
  flashXfer.cmdType       = kFLEXSPI_Command;
  flashXfer.SeqNumber     = 1;
  flashXfer.seqIndex      = NOR_CMD_LUT_SEQ_IDX_ERASESECTOR;
  status                  = FLEXSPI_TransferBlocking(FLEXSPI, &flashXfer);

  if(status != kStatus_Success)
  {
    return status;
  }

  status = QSP_flexspi_nor_wait_bus_busy();

  // Do software reset. 
  FLEXSPI_SoftwareReset(FLEXSPI);

  return status;
}

/***************************************************************************//**
* FuncName: {QSP_flexspi_nor_flash_page_program}
*
* @brief This function programs a flash page.
*
* @details This function programs a flash page.
*
* @todo None
*
* IN:
* @param[in] uint32_t dstAddr: mandatory, memory page address
* @param[in] uint32_t *src: mandatory, buffer address
*            
*
* OUT:
* @param[out] None
*
* RET:
* @retval status_t status
*
*
* @author {Luca Tollot} @date {08/01/2020}
******************************************************************************/

status_t QSP_flexspi_nor_flash_page_program(uint32_t dstAddr, uint32_t *src)
{
  status_t status;
  flexspi_transfer_t flashXfer;

  // Write enable 
  status = QSP_flexspi_nor_write_enable(dstAddr);

  if(status != kStatus_Success)
  {
    return status;
  }

  // Prepare page program command 
  flashXfer.deviceAddress = dstAddr;
  flashXfer.port          = kFLEXSPI_PortA1;
  flashXfer.cmdType       = kFLEXSPI_Write;
  flashXfer.SeqNumber     = 1;
  flashXfer.seqIndex      = NOR_CMD_LUT_SEQ_IDX_PAGEPROGRAM_QUAD;
  flashXfer.data          = (uint32_t *)src;
  flashXfer.dataSize      = QSP_FLASH_PAGE_SIZE;
  status                  = FLEXSPI_TransferBlocking(FLEXSPI, &flashXfer);

  if(status != kStatus_Success)
  {
    return status;
  }

  status = QSP_flexspi_nor_wait_bus_busy();

  // Do software reset. 
  FLEXSPI_SoftwareReset(FLEXSPI);
  
  DCACHE_CleanInvalidateByRange(dstAddr, QSP_FLASH_PAGE_SIZE);

  return status;
}

/***************************************************************************//**
* FuncName: {QSP_flexspi_nor_flash_program}
*
* @brief This function programs a number of byte.
*
* @details This function programs a number of byte.
*
* @todo None
*
* IN:
* @param[in] uint32_t dstAddr: mandatory, memory page address
* @param[in] uint32_t *src: mandatory, buffer address
* @param[in] size_t size: mandatory, size buffer
*            
*
* OUT:
* @param[out] None
*
* RET:
* @retval status_t status
*
*
* @author {Luca Tollot} @date {08/01/2020}
******************************************************************************/

status_t QSP_flexspi_nor_flash_program(uint32_t dstAddr, uint32_t *src, size_t size)
{
  status_t status;
  flexspi_transfer_t flashXfer;

  // Write enable 
  status = QSP_flexspi_nor_write_enable(dstAddr);

  if(status != kStatus_Success)
  {
    return status;
  }

  // Prepare page program command 
  flashXfer.deviceAddress = dstAddr;
  flashXfer.port          = kFLEXSPI_PortA1;
  flashXfer.cmdType       = kFLEXSPI_Write;
  flashXfer.SeqNumber     = 1;
  flashXfer.seqIndex      = NOR_CMD_LUT_SEQ_IDX_PAGEPROGRAM_QUAD;
  flashXfer.data          = (uint32_t *)src;
  flashXfer.dataSize      = size;
  status                  = FLEXSPI_TransferBlocking(FLEXSPI, &flashXfer);

  if(status != kStatus_Success)
  {
    return status;
  }

  status = QSP_flexspi_nor_wait_bus_busy();

  // Do software reset. 
  FLEXSPI_SoftwareReset(FLEXSPI);
  
  DCACHE_CleanInvalidateByRange(dstAddr, size);

  return status;
}

/***************************************************************************//**
* FuncName: {QSP_flexspi_nor_get_vendor_id}
*
* @brief This function get a vendor ID.
*
* @details This function get a vendor ID.
*
* @todo None
*
* IN:
* @param[in] uint8_t *vendorId: mandatory, address vendor ID
*            
*
* OUT:
* @param[out] None
*
* RET:
* @retval status_t status
*
*
* @author {Luca Tollot} @date {08/01/2020}
******************************************************************************/

status_t QSP_flexspi_nor_get_vendor_id(uint8_t *vendorId)
{
  uint32_t temp;
  flexspi_transfer_t flashXfer;
  
  flashXfer.deviceAddress = 0;
  flashXfer.port          = kFLEXSPI_PortA1;
  flashXfer.cmdType       = kFLEXSPI_Read;
  flashXfer.SeqNumber     = 1;
  flashXfer.seqIndex      = NOR_CMD_LUT_SEQ_IDX_READID;
  flashXfer.data          = &temp;
  flashXfer.dataSize      = 1;

  status_t status = FLEXSPI_TransferBlocking(FLEXSPI, &flashXfer);

  *vendorId = temp;

  // Do software reset. 
  FLEXSPI_SoftwareReset(FLEXSPI);

  return status;
}

/***************************************************************************//**
* FuncName: {QSP_flexspi_nor_erase_chip}
*
* @brief This function erase all chip.
*
* @details This function erase all chip.
*
* @todo None
*
* IN:
* @param[in] None
*            
*
* OUT:
* @param[out] None
*
* RET:
* @retval status_t status
*
*
* @author {Luca Tollot} @date {08/01/2020}
******************************************************************************/

status_t QSP_flexspi_nor_erase_chip()
{
  status_t status;
  flexspi_transfer_t flashXfer;

  // Write enable 
  status = QSP_flexspi_nor_write_enable(0);

  if(status != kStatus_Success)
  {
    return status;
  }

  flashXfer.deviceAddress = 0;
  flashXfer.port          = kFLEXSPI_PortA1;
  flashXfer.cmdType       = kFLEXSPI_Command;
  flashXfer.SeqNumber     = 1;
  flashXfer.seqIndex      = NOR_CMD_LUT_SEQ_IDX_ERASECHIP;

  status = FLEXSPI_TransferBlocking(FLEXSPI, &flashXfer);

  if(status != kStatus_Success)
  {
    return status;
  }

  status = QSP_flexspi_nor_wait_bus_busy(FLEXSPI);

  return status;
}

/***************************************************************************//**
* FuncName: {QSP_Sector_Program}
*
* @brief This function erase and programs a flash sector.
*
* @details This function erase and programs a flash sector.
*
* @todo None
*
* IN:
* @param[in] uint32_t nsector: mandatory, sector number
* @param[in] uint32_t *src: mandatory, buffer address
*            
*
* OUT:
* @param[out] None
*
* RET:
* @retval status_t status
*
*
* @author {Luca Tollot} @date {24/01/2020}
******************************************************************************/

status_t QSP_Sector_Program(uint32_t nsector, uint32_t *src)
{
  status_t status;
  uint16_t i;
  
  status = QSP_flexspi_nor_flash_erase_sector(nsector); 
  
  if(status != kStatus_Success)
  {
    return status;
  }
  
  for(i = 0;i < QSP_NUM_SECTOR_PAGE;i++)
  {
    status = QSP_flexspi_nor_flash_page_program(nsector * QSP_SECTOR_SIZE + i * QSP_FLASH_PAGE_SIZE, (void*)src); //erase page image vector table
    
    if(status != kStatus_Success)
    {
      return status;
    }  

    DCACHE_CleanInvalidateByRange(nsector * QSP_SECTOR_SIZE + i * QSP_FLASH_PAGE_SIZE, QSP_FLASH_PAGE_SIZE);
  }
  
  return status;
}


