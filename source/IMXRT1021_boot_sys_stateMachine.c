/***************************************************************************//**
* @file IMXRT1021_boot_sys_stateMachine.c
*
* @brief Sys Service Routines.
*
* @details This file handles system state machine.
*
*
* @todo None
*
*
* @author Luca Tollot @date {29/12/2019}
******************************************************************************/

/* Includes ------------------------------------------------------------------*/
#include "IMXRT1021_boot_main_ram.h"
 




/***************************************************************************//**
* FuncName: {SYS_Init}
*
* @brief This function initializes state machine object.
*
* @details This function initializes state machine object and returns state
*          of initialization.
*
* @todo None
*
* IN:
* @param[in] None
*            
*
* OUT:
* @param[out] None
*
* RET:
* @retval <OK> 0
* @retval <NOK> -1
*
*
* @author {Luca Tollot} @date {20/09/2019}
******************************************************************************/

int16_t SYS_Init()
{
  //uint16_t i;
  
  sysStateMachineObj.timeOutMachine = SYS_TIME_OUT_MACHINE;
  
  sysStateMachineObj.state = SYS_INIT_STATE;
  
  sysStateMachineObj.info.rel = RELEASE;
  
  sysStateMachineObj.info.subrel = SUBRELEASE;
    
  sysStateMachineObj.info.day = DATA_DAY;
  
  sysStateMachineObj.info.month = DATA_MONTH;
  
  sysStateMachineObj.info.year = DATA_YEAR;
  
  sysStateMachineObj.info.fam = FAMILY;  
  
  sysStateMachineObj.enableTimeOutBootLife = true;
  
  return 0;
}

/***************************************************************************//**
* FuncName: {SYS_StateMachine}
*
* @brief This function manages the state machine.
*
* @details This function manages the state machine.
*
* @todo None
*
* IN:
* @param[in] None
*            
*
* OUT:
* @param[out] sysState sysStateMachine->state: state machine
*
* RET:
* @retval None
*
*
* @author {Luca Tollot} @date {13/01/2019}
******************************************************************************/

void SYS_StateMachine()
{
  //uint16_t crc;
  //uint16_t i, k;
  
  if(sysStateMachineObj.state != SYS_INIT_STATE)
  {
    if(sysStateMachineObj.timeOutMachine == 0) //Condizione di timeout
    {   
      sysStateMachineObj.state = SYS_RESET_STATE;
      sysStateMachineObj.timeOutMachine = SYS_TIME_OUT_MACHINE;
    }
  }
  
  switch(sysStateMachineObj.state)
  {
    case SYS_INIT_STATE:
    {
      SYS_Init();
      
      if(SYS_Read_Entry_Table()) //Lettura entry table
      {
        sysStateMachineObj.timeOutBootLife = qspControlStatusObj.entryTable.entryTableStruct.timeOutBootLife;
      }
      else
      {
        qspControlStatusObj.entryTable.entryTableStruct.numberImages = 0;
        qspControlStatusObj.entryTable.entryTableStruct.selImage = 0xFF;
        qspControlStatusObj.entryTable.entryTableStruct.timeOutBootLife = TIME_OUT_BOOT_LIFE_DEFAULT;
       
        qspControlStatusObj.entryTable.entryTableStruct.serialOptions.addr = URT_DEFAULT_ADDR; //Default serial address
        
        qspControlStatusObj.entryTable.entryTableStruct.serialOptions.padd1 = 0x00; //padding
        qspControlStatusObj.entryTable.entryTableStruct.serialOptions.padd2 = 0x00; //padding
            
        qspControlStatusObj.entryTable.entryTableStruct.serialOptions.baudRate = URT_DEFAULT_BAUD_RATE; //Default baud rate
        
        SYS_Store_Entry_Table();
        
        sysStateMachineObj.enableTimeOutBootLife = false; //Disable boot life
      }
      
      sysStateMachineObj.state = SYS_WAIT_STATE;
      
      
      sysStateMachineObj.timeOutMachine = SYS_TIME_OUT_MACHINE;
    }
    break;
    
    case SYS_WAIT_STATE:
    {      
      if(sysStateMachineObj.enableTimeOutBootLife == true) //Enable time out boot life
      {
        if(qspControlStatusObj.entryTable.entryTableStruct.selImage != 0xFF && sysStateMachineObj.timeOutBootLife == 0) //Condizione di timeout
        {      
          //entryPoint = qspControlStatusObj.entryTable.entryTableStruct.infoImage[qspControlStatusObj.entryTable.entryTableStruct.selImage - 1].addr;
          
          //sysStateMachineObj.flashPointer = (uint8_t*)QSP_ADDR_BOOT_FLASH;
  
          //entryPoint = UTY_KMPSearch((uint8_t*)pattern, sizeof(pattern), sysStateMachineObj.flashPointer, 60000); 
          //entryPoint++;
          
          entryPoint = qspControlStatusObj.entryTable.entryTableStruct.infoImage[qspControlStatusObj.entryTable.entryTableStruct.selImage].addr;
          
          __disable_irq();
          DisableIRQ(PIT_IRQ_ID);
          
          asm("LDR R0, =entryPoint");
          asm("LDR R0, [R0]");
          asm("BX R0"); //Jump to image
        }
        else if(qspControlStatusObj.entryTable.entryTableStruct.selImage == 0xFF)
          sysStateMachineObj.enableTimeOutBootLife = false;
      }
      
      sysStateMachineObj.timeOutMachine = SYS_TIME_OUT_MACHINE;
    }
    break;
    
    case SYS_UPLOAD_STATE:
    { 
      if(sysStateMachineObj.endUploadPck) //wait end receive packet
      {
        /*memcpy(qspControlStatusObj.sectorBuffer.buffer, (void*)(QSP_FLEXSPI_AMBA_BASE + qspControlStatusObj.newImg.startSector * QSP_SECTOR_SIZE + QSP_OFFSET_IMAGE_VECTOR_TABLE), sizeof(qspControlStatusObj.sectorBuffer.buffer)); //lettura image vector table
        
        for(k = 0;k < 8;k++)
        {
          if(qspControlStatusObj.sectorBuffer.buffer32[k] >= 0x60000000 && qspControlStatusObj.sectorBuffer.buffer32[k] != 0x00000000 && qspControlStatusObj.sectorBuffer.buffer32[k] != 0xFFFFFFFF)
            qspControlStatusObj.sectorBuffer.buffer32[k] += qspControlStatusObj.newImg.startSector * QSP_SECTOR_SIZE;
        }
        
        QSP_Sector_Program(qspControlStatusObj.newImg.startSector + 1, (void*)qspControlStatusObj.sectorBuffer.buffer);
        
        
        memcpy(qspControlStatusObj.sectorBuffer.buffer, (void*)(QSP_FLEXSPI_AMBA_BASE + qspControlStatusObj.newImg.startSector * QSP_SECTOR_SIZE + QSP_OFFSET_VECTOR_TABLE), sizeof(qspControlStatusObj.sectorBuffer.buffer)); //lettura vector table
        
        for(k = 0;k < QSP_SIZE_INTERRUPT;k++)
        {
          if(qspControlStatusObj.sectorBuffer.buffer32[k] >= 0x60000000 && qspControlStatusObj.sectorBuffer.buffer32[k] != 0x00000000 && qspControlStatusObj.sectorBuffer.buffer32[k] != 0xFFFFFFFF)
            qspControlStatusObj.sectorBuffer.buffer32[k] += qspControlStatusObj.newImg.startSector * QSP_SECTOR_SIZE;
        }
        
        QSP_Sector_Program(qspControlStatusObj.newImg.startSector + 2, (void*)qspControlStatusObj.sectorBuffer.buffer);*/
        
        
        /*for(i = 0;i < qspControlStatusObj.newImg.numSector;i++)
        {
          memcpy(qspControlStatusObj.sectorBuffer.buffer, (void*)(QSP_FLEXSPI_AMBA_BASE + qspControlStatusObj.newImg.startSector * QSP_SECTOR_SIZE + i * QSP_SECTOR_SIZE), sizeof(qspControlStatusObj.sectorBuffer.buffer)); //lettura sector
        
          for(k = 0;k < QSP_SIZE_INTERRUPT;k++)
          {
            if(qspControlStatusObj.sectorBuffer.buffer32[k] >= 0x60000000 && qspControlStatusObj.sectorBuffer.buffer32[k] < 0x70000000 && qspControlStatusObj.sectorBuffer.buffer32[k] != 0x00000000 && qspControlStatusObj.sectorBuffer.buffer32[k] != 0xFFFFFFFF)
              qspControlStatusObj.sectorBuffer.buffer32[k] += qspControlStatusObj.newImg.startSector * QSP_SECTOR_SIZE;
          }
        
          QSP_Sector_Program(qspControlStatusObj.newImg.startSector + i, (void*)qspControlStatusObj.sectorBuffer.buffer);
        }*/
        
        
        /*memcpy(qspControlStatusObj.imageVectorTable.buffer, (void*)(QSP_FLEXSPI_AMBA_BASE + qspControlStatusObj.newImg.startSector * QSP_SECTOR_SIZE + QSP_OFFSET_IMAGE_VECTOR_TABLE), sizeof(qspControlStatusObj.imageVectorTable.buffer)); //lettura image vector table
        
        if(qspControlStatusObj.imageVectorTable.structure.entry != 0x00000000 && qspControlStatusObj.imageVectorTable.structure.entry != 0xFFFFFFFF)
          qspControlStatusObj.imageVectorTable.structure.entry += qspControlStatusObj.newImg.startSector * QSP_SECTOR_SIZE;
        if(qspControlStatusObj.imageVectorTable.structure.dcd != 0x00000000 && qspControlStatusObj.imageVectorTable.structure.dcd != 0xFFFFFFFF)
          qspControlStatusObj.imageVectorTable.structure.dcd += qspControlStatusObj.newImg.startSector * QSP_SECTOR_SIZE;
        if(qspControlStatusObj.imageVectorTable.structure.boot_data != 0x00000000 && qspControlStatusObj.imageVectorTable.structure.boot_data != 0xFFFFFFFF)
          qspControlStatusObj.imageVectorTable.structure.boot_data += qspControlStatusObj.newImg.startSector * QSP_SECTOR_SIZE;
        if(qspControlStatusObj.imageVectorTable.structure.self != 0x00000000 && qspControlStatusObj.imageVectorTable.structure.self != 0xFFFFFFFF)
          qspControlStatusObj.imageVectorTable.structure.self += qspControlStatusObj.newImg.startSector * QSP_SECTOR_SIZE;
        if(qspControlStatusObj.imageVectorTable.structure.csf != 0x00000000 && qspControlStatusObj.imageVectorTable.structure.csf != 0xFFFFFFFF)
          qspControlStatusObj.imageVectorTable.structure.csf += qspControlStatusObj.newImg.startSector * QSP_SECTOR_SIZE;
        
        memset ((void*)qspControlStatusObj.buffer, 0xFF, sizeof(qspControlStatusObj.buffer));
        
        if(QSP_flexspi_nor_flash_page_program(qspControlStatusObj.newImg.startSector * QSP_SECTOR_SIZE + QSP_OFFSET_IMAGE_VECTOR_TABLE, (void*)qspControlStatusObj.buffer) == -1) //erase page image vector table
          sysStateMachineObj.errCode = 3; //Error write image vector table
    
        if(QSP_flexspi_nor_flash_page_program(qspControlStatusObj.newImg.startSector * QSP_SECTOR_SIZE + QSP_OFFSET_IMAGE_VECTOR_TABLE, (void*)qspControlStatusObj.imageVectorTable.buffer) == -1) //scrittura image vector table
          sysStateMachineObj.errCode = 3; //Error write image vector table
        
        
        for(i = 0;i < 4;i++) //Aggiornamento tabella interrupt
        {
          memcpy(qspControlStatusObj.vectorTable, (void*)(QSP_FLEXSPI_AMBA_BASE + qspControlStatusObj.newImg.startSector * QSP_SECTOR_SIZE + QSP_OFFSET_VECTOR_TABLE + i * QSP_FLASH_PAGE_SIZE), QSP_FLASH_PAGE_SIZE); //lettura vector table
          
          for(k = 0;k < (QSP_SIZE_INTERRUPT / 4);k++)
          {
            if(qspControlStatusObj.vectorTable[k] >= 0x60000000 && qspControlStatusObj.vectorTable[k] != 0x00000000 && qspControlStatusObj.vectorTable[k] != 0xFFFFFFFF)
              qspControlStatusObj.vectorTable[k] += qspControlStatusObj.newImg.startSector * QSP_SECTOR_SIZE;
          }
          
          if(QSP_flexspi_nor_flash_page_program(qspControlStatusObj.newImg.startSector * QSP_SECTOR_SIZE + QSP_OFFSET_IMAGE_VECTOR_TABLE + i * QSP_FLASH_PAGE_SIZE, (void*)qspControlStatusObj.vectorTable) == -1) //scrittura image vector table
            sysStateMachineObj.errCode = 4; //Error write vector table
        }
        */
        
       
        sysStateMachineObj.flashPointer = (uint8_t*)(QSP_FLEXSPI_AMBA_BASE + qspControlStatusObj.newImg.startSector * QSP_SECTOR_SIZE + (QSP_OFFSET_VECTOR_TABLE + 4));
        qspControlStatusObj.newImg.addr = (*(sysStateMachineObj.flashPointer + 0) << 0) | (*(sysStateMachineObj.flashPointer + 1) << 8) | (*(sysStateMachineObj.flashPointer + 2) << 16) | (*(sysStateMachineObj.flashPointer + 3) << 24);
        
        qspControlStatusObj.entryTable.entryTableStruct.infoImage[qspControlStatusObj.newImg.nImage].addr = qspControlStatusObj.newImg.addr;
        qspControlStatusObj.entryTable.entryTableStruct.infoImage[qspControlStatusObj.newImg.nImage].startSector = qspControlStatusObj.newImg.startSector;
        qspControlStatusObj.entryTable.entryTableStruct.infoImage[qspControlStatusObj.newImg.nImage].numSector = qspControlStatusObj.newImg.numSector;
        qspControlStatusObj.entryTable.entryTableStruct.infoImage[qspControlStatusObj.newImg.nImage].size = qspControlStatusObj.newImg.size;
          
        SYS_Store_Entry_Table();
        
        sysStateMachineObj.endUploadPck = false;
        
        sysStateMachineObj.state = SYS_WAIT_STATE;
        sysStateMachineObj.enableTimeOutBootLife = true;
      }
      
      sysStateMachineObj.timeOutMachine = SYS_TIME_OUT_MACHINE;
    }
    break;
    
    case SYS_DOWNLOAD_STATE:
    {      

      
      sysStateMachineObj.timeOutMachine = SYS_TIME_OUT_MACHINE;
    }
    break;
       
    case SYS_ERROR_STATE:
    {      
      sysStateMachineObj.errCode = 0; //No error
    }
    break;
    
    case SYS_RESET_STATE:
    {      
      Reset_Handler();
    }
    break;
      
    default:
    {
      Reset_Handler();
    }
    break;
      
  }
}

/***************************************************************************//**
* FuncName: {SYS_Read_Entry_Table}
*
* @brief This function read a entry table.
*
* @details This function read a entry table.
*
* @todo None
*
* IN:
* @param[in] None
*            
*
* OUT:
* @param[out] None
*
* RET:
* @retval true: read ok
*         false: read fail
*
*
* @author {Luca Tollot} @date {23/01/2020}
******************************************************************************/

bool SYS_Read_Entry_Table()
{
  uint16_t crc;
  
  memcpy(qspControlStatusObj.entryTable.buffer, (void*)(QSP_FLEXSPI_AMBA_BASE + QSP_ENTRY_TABLE_ADDR), sizeof(qspControlStatusObj.entryTable.buffer)); //lettura tabella entry point
      
  crc = UTY_Crc16Tab(qspControlStatusObj.entryTable.buffer, sizeof(qspControlStatusObj.entryTable.buffer) - QSP_CRC_SIZE_ENTRY_TABLE); //calcolo crc entry table
      
  if(crc == qspControlStatusObj.entryTable.entryTableStruct.crc && qspControlStatusObj.entryTable.entryTableStruct.numberImages <= QSP_MAX_NUMBER_IMAGES) //Controllo crc e valori
    return true;
  else return false;
}

/***************************************************************************//**
* FuncName: {SYS_Store_Entry_Table}
*
* @brief This function store a entry table.
*
* @details This function store a entry table.
*
* @todo None
*
* IN:
* @param[in] None
*            
*
* OUT:
* @param[out] None
*
* RET:
* @retval None
*
*
* @author {Luca Tollot} @date {23/01/2020}
******************************************************************************/

void SYS_Store_Entry_Table()
{
  uint16_t crc;
  
  if(QSP_flexspi_nor_flash_erase_sector(QSP_ENTRY_TABLE_SECTOR) == -1) //Settore con tabella entry point
    sysStateMachineObj.errCode = 1; //Error erase sector

  crc = UTY_Crc16Tab(qspControlStatusObj.entryTable.buffer, sizeof(qspControlStatusObj.entryTable.buffer) - QSP_CRC_SIZE_ENTRY_TABLE); //calcolo crc entry table
        
  qspControlStatusObj.entryTable.entryTableStruct.crc = crc;     
        
  if(QSP_flexspi_nor_flash_page_program(QSP_ENTRY_TABLE_ADDR, (void*)qspControlStatusObj.entryTable.buffer) == -1) //scrittura tabella entry point
    sysStateMachineObj.errCode = 2; //Error write entry point table
}
