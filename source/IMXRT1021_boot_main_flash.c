/*
 * Copyright (c) 2013 - 2016, Freescale Semiconductor, Inc.
 * Copyright 2016-2018 NXP
 * All rights reserved.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

/*******************************************************************************
 * Includes
 ******************************************************************************/

#include "IMXRT1021_boot_main_flash.h"


/*******************************************************************************
 * Definitions
 ******************************************************************************/

/*******************************************************************************
* Prototypes
******************************************************************************/

/*******************************************************************************
* Variables
******************************************************************************/

uint16_t i;
uint8_t *pointer;
uint32_t entryPoint;
uint8_t *flashPointer;

extern const uint8_t bootloader_ram[];


/*******************************************************************************
 * Code
 ******************************************************************************/



/*!
 * @brief Main function.
 */
void main(void)
{
  /*flashPointer = (uint8_t*)ADDR_BOOT_FLASH;
  
  entryPoint = UTY_KMPSearch((uint8_t*)pattern, sizeof(pattern), flashPointer, SIZE); 
  entryPoint++;*/
  
  flashPointer = (uint8_t*)(ADDR_BOOT_FLASH + 4);
  entryPoint = (*(flashPointer + 0) << 0) | (*(flashPointer + 1) << 8) | (*(flashPointer + 2) << 16) | (*(flashPointer + 3) << 24);
  
  pointer = (uint8_t*)STARTADDR;
    
  for(i = 0;i < SIZE;i++)
    *(pointer++) = bootloader_ram[i];     
    
  asm("LDR R0, =entryPoint");
  asm("LDR R0, [R0]");
  asm("BX R0");
}














