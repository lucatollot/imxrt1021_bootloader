/***************************************************************************//**
* @file IMXRT1021_boot_uty_stateMachine.c
*
* @brief Uty Service Routines.
*
* @details This file handles utility
*
*
* @todo None
*
*
* @author Luca Tollot @date {23/09/2019}
******************************************************************************/

/* Includes ------------------------------------------------------------------*/

#ifdef BOOT_FLASH
#include "IMXRT1021_boot_main_flash.h"
#else
#include "IMXRT1021_boot_main_ram.h"
#endif




/***************************************************************************//**
* FuncName: {UTY_Crc16Tab}
*
* @brief This function calculates crc16.
*
* @details This function calculates crc16.
*
* @todo None
*
* IN:
* @param[in] uint8_t[] *buffer: mandatory, buffer data
* @param[in] uint32_t length: mandatory, buffer length
*            
*
* OUT:
* @param[out] None
*
* RET:
* @retval crc16
*
*
* @author {Luca Tollot} @date {23/09/2019}
******************************************************************************/

uint16_t UTY_Crc16Tab(uint8_t *buffer, uint32_t length)
{
  uint16_t crc;
  uint32_t i;
  
  crc = UTY_CRC_PRESET16;

  for(i = 0;i < length;i++)
    crc = (crc >> 8) ^ tableCRC16[(crc ^ *(buffer + i)) & 0xFF];

  return crc;
}

/***************************************************************************//**
* FuncName: {UTY_Crc16}
*
* @brief This function calculates crc16.
*
* @details This function calculates crc16.
*
* @todo None
*
* IN:
* @param[in] uint8_t[] *buffer: mandatory, buffer data
* @param[in] uint32_t length: mandatory, buffer length
*            
*
* OUT:
* @param[out] None
*
* RET:
* @retval crc16
*
*
* @author {Luca Tollot} @date {23/09/2019}
******************************************************************************/

uint16_t UTY_Crc16(uint8_t *buffer, uint32_t length)
{
  uint16_t crc;
  uint32_t i;
  uint8_t j;
  
  crc = UTY_CRC_PRESET16;

  //Polynomial: x16 + x12 + x5 + 1 
  //Start Value: 0xFFFF 

  for(i = 0; i < length; i++)
  {
    crc ^= *(buffer + i);
    
    for(j = 0; j < 8; j++)
    {
      if(crc & 0x0001)
      {
        crc = (crc >> 1) ^ UTY_CRC_POLYNOM16;
      }
      else crc = (crc >> 1);
    }
  }
  
  return crc;
} 

/***************************************************************************//**
* FuncName: {UTY_Crc32Tab}
*
* @brief This function calculates crc32.
*
* @details This function calculates crc32.
*
* @todo None
*
* IN:
* @param[in] uint8_t[] *buffer: mandatory, buffer data
* @param[in] uint32_t length: mandatory, buffer length
*            
*
* OUT:
* @param[out] None
*
* RET:
* @retval crc32
*
*
* @author {Luca Tollot} @date {23/09/2019}
******************************************************************************/

/*uint32_t UTY_Crc32Tab(uint8_t *buffer, uint32_t length)
{
  uint32_t crc = 0xFFFFFFFF;
  uint32_t i;

  for(i = 0;i < length;i++)
    crc = (crc >> 8) ^ tableCRC32[(crc ^ (uint32_t)*(buffer + i)) & 0xFF];
        
  return crc;
}*/

/***************************************************************************//**
* FuncName: {UTY_UpdateCrc32Tab}
*
* @brief This function update crc32.
*
* @details This function update crc32.
*
* @todo None
*
* IN:
* @param[in] uint8_t[] *buffer: mandatory, buffer data
* @param[in] uint32_t length: mandatory, buffer length
*            
*
* OUT:
* @param[out] None
*
* RET:
* @retval crc32
*
*
* @author {Luca Tollot} @date {23/09/2019}
******************************************************************************/

/*uint32_t UTY_UpdateCrc32Tab(uint8_t *buffer, uint32_t length)
{
  uint32_t i;

  for(i = 0;i < length;i++)
    utyControlStatusObj.crc32 = (utyControlStatusObj.crc32 >> 8) ^ tableCRC32[(utyControlStatusObj.crc32 ^ (uint32_t)*(buffer + i)) & 0xFF];
        
  return utyControlStatusObj.crc32;
}*/

/***************************************************************************//**
* FuncName: {UTY_PaddingCrc32Tab}
*
* @brief This function update crc32.
*
* @details This function update crc32.
*
* @todo None
*
* IN:
* @param[in] uint32_t length: mandatory, buffer length
*            
*
* OUT:
* @param[out] None
*
* RET:
* @retval crc32
*
*
* @author {Luca Tollot} @date {23/09/2019}
******************************************************************************/

/*uint32_t UTY_PaddingCrc32Tab(uint32_t length)
{
  uint32_t i;

  for(i = 0;i < length;i++)
    utyControlStatusObj.crc32 = (utyControlStatusObj.crc32 >> 8) ^ tableCRC32[(utyControlStatusObj.crc32 ^ 0xFF) & 0xFF];
        
  return utyControlStatusObj.crc32;
}*/

/***************************************************************************//**
* FuncName: {UTY_Crc32}
*
* @brief This function calculates crc32.
*
* @details This function calculates crc32.
*
* @todo None
*
* IN:
* @param[in] uint8_t[] *buffer: mandatory, buffer data
* @param[in] uint32_t length: mandatory, buffer length
*            
*
* OUT:
* @param[out] None
*
* RET:
* @retval crc32
*
*
* @author {Luca Tollot} @date {23/09/2019}
******************************************************************************/

/*uint32_t UTY_Crc32(uint8_t *buffer, uint32_t length)
{
  uint32_t crc = UTY_CRC_PRESET32;
  uint32_t i, j;

  for(j = 0;j < length;j++) 
  {
    uint32_t temp = (crc ^ (uint32_t)buffer[j]) & 0xFF;

    // read 8 bits one at a time
    for(i = 0; i < 8; i++) 
    {
      if((temp & 1) == 1) 
      {
        temp = (temp >> 1) ^ UTY_CRC_POLYNOM32;
      }
      else temp = (temp >> 1);
    }
            
    crc = (crc >> 8) ^ temp;
  }
  
  return crc;
}*/

/***************************************************************************//**
* FuncName: {UTY_CrcMemory}
*
* @brief This function calculates crc32.
*
* @details This function calculates crc32.
*
* @todo None
*
* IN:
* @param[in] uint32_t[] addrStart: mandatory, memory address
* @param[in] uint32_t length: mandatory, buffer length
*            
*
* OUT:
* @param[out] None
*
* RET:
* @retval crc32
*
*
* @author {Luca Tollot} @date {23/09/2019}
******************************************************************************/

/*uint32_t UTY_CrcMemory(uint32_t addrStart, uint32_t length)
{
  uint32_t i;
  const char *img = (const char*)addrStart;
  
  utyControlStatusObj.crc32 = UTY_CRC_PRESET32;
  
  for(i = 0;i < length;i++)
    utyControlStatusObj.crc32 = (utyControlStatusObj.crc32 >> 8) ^ tableCRC32[(utyControlStatusObj.crc32 ^ (uint32_t)*(img + i)) & 0xFF];
        
  return utyControlStatusObj.crc32;
}*/

/***************************************************************************//**
* FuncName: {UTY_Round}
*
* @brief This function calculates round-off conversion.
*
* @details This function calculates round-off conversion.
*
* @todo None
*
* IN:
* @param[in] int16_t[] value: mandatory, input value
*            
*
* OUT:
* @param[out] None
*
* RET:
* @retval output value
*
*
* @author {Luca Tollot} @date {23/09/2019}
******************************************************************************/

int16_t UTY_Round(int16_t value)
{
  uint16_t temp;

  // get positive value
  temp = value;
  
  if(value < 0)
    temp = (uint16_t)~(uint16_t)value + 1;
	
  // round-off
  temp = (uint16_t)temp + 4;
  temp = (uint16_t)(temp / 10) * 10;
    
  if(value < 0)
    value = ~(uint16_t)temp + 1;
  else value = (uint16_t)temp;
	
  return value;
}

/***************************************************************************//**
* FuncName: {UTY_Round_Up}
*
* @brief This function calculates round-off up conversion.
*
* @details This function calculates round-off up conversion.
*
* @todo None
*
* IN:
* @param[in] int16_t[] value: mandatory, input value
*            
*
* OUT:
* @param[out] None
*
* RET:
* @retval output value
*
*
* @author {Luca Tollot} @date {23/09/2019}
******************************************************************************/

int16_t UTY_Round_Up(int16_t value)
{
  int16_t temp;

  temp = abs(value % 10);
  
  if(value < 0)
    return (value + temp);
  else return (value + (10 - temp));
}

/***************************************************************************//**
* FuncName: {UTY_Round_Down}
*
* @brief This function calculates round-off down conversion.
*
* @details This function calculates round-off down conversion.
*
* @todo None
*
* IN:
* @param[in] int16_t[] value: mandatory, input value
*            
*
* OUT:
* @param[out] None
*
* RET:
* @retval output value
*
*
* @author {Luca Tollot} @date {23/09/2019}
******************************************************************************/

int16_t UTY_Round_Down(int16_t value)
{
  int16_t temp;

  temp = abs(value % 10);
  
  if(value < 0)
    return (value + (10 - temp));
  else return (value - temp);
}

/***************************************************************************//**
* FuncName: {UTY_DelayTime}
*
* @brief This function perform a delay.
*
* @details This function perform a delay.
*
* @todo None
*
* IN:
* @param[in] uint8_t time: mandatory, input value
*            
*
* OUT:
* @param[out] None
*
* RET:
* @retval None
*
*
* @author {Luca Tollot} @date {23/09/2019}
******************************************************************************/

void UTY_DelayTime(uint8_t time)
{ 
  uint8_t i = 0;

  while(i < time)
    i++;
}

/***************************************************************************//**
* FuncName: {UTY_KMPSearch}
*
* @brief This function find a pattern position in a text.
*
* @details This function find a pattern position in a text using KMP algorithm.
*
* @todo None
*
* IN:
* @param[in] uint8_t* pat: mandatory, pattern pointer
* @param[in] uint16_t pat_length: mandatory, pattern length
* @param[in] uint8_t* txt: mandatory, text pointer
* @param[in] uint16_t txt_length: mandatory, text length
*            
*
* OUT:
* @param[out] None
*
* RET:
* @retval None
*
*
* @author {Luca Tollot} @date {13/01/2020}
******************************************************************************/

int16_t UTY_KMPSearch(uint8_t* pat, uint16_t pat_length, uint8_t* txt, uint16_t txt_length) 
{ 
  uint16_t len = 0; 
  uint16_t i = 1; 
  //uint16_t i = 0; // index for txt[] 
  uint16_t j = 0; // index for pat[]  
  //uint16_t lps[30];
  uint16_t *lps;
  
  lps = (uint16_t*) malloc(sizeof(uint16_t) * pat_length);
  
  lps[0] = 0; 

  while(i < pat_length) 
  { 
    if(pat[i] == pat[len]) 
    { 
      len++; 
      lps[i] = len; 
      i++; 
    } 
    else 
    { 
      if(len != 0) 
        len = lps[len - 1]; 
      else 
      { 
        lps[i] = 0; 
        i++; 
      } 
    } 
  } 
  
  i = 0;
  
  while(i < txt_length) 
  { 
    if(pat[j] == txt[i]) 
    { 
      j++; 
      i++; 
    } 
  
    if(j == pat_length) 
    {
      return (i - j);
      //j = lps[j - 1]; 
    }
    else if (i < txt_length && pat[j] != txt[i]) 
    { 
      if(j != 0) 
        j = lps[j - 1]; 
      else i = i + 1; 
    } 
  }
  
  return -1;
}
