/*
 * Copyright (c) 2013 - 2016, Freescale Semiconductor, Inc.
 * Copyright 2016-2018 NXP
 * All rights reserved.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

/*******************************************************************************
 * Includes
 ******************************************************************************/
//#include "fsl_debug_console.h"
#include "board.h"
#include "fsl_common.h"
//#include "fsl_rtwdog.h"
//#include "fsl_wdog.h"
#include <intrinsics.h>
#include "fsl_cache.h"

#include "pin_mux.h"
#include "clock_config.h"

#include "IMXRT1021_boot_main_ram.h"



/*******************************************************************************
 * Definitions
 ******************************************************************************/

/*******************************************************************************
* Prototypes
******************************************************************************/

/*******************************************************************************
* Variables
******************************************************************************/

timControlStatus_t timControlStatusObj;   
utyControlStatus_t utyControlStatusObj;
qspControlStatus_t qspControlStatusObj;

sysStateMachine_t sysStateMachineObj;
AT_NONCACHEABLE_SECTION_INIT(urtStateMachine_t urtStateMachineObj[2]);


uint32_t entryPoint;



//rtwdog_config_t rtwdogconfig;
//wdog_config_t wdogconfig;


//status_t status;
//static uint8_t s_nor_program_buffer[256];
//uint16_t i;



/*******************************************************************************
 * Code
 ******************************************************************************/


   
/*void RTWDOG_IRQHandler(void)
{
  RTWDOG_ClearStatusFlags(RTWDOG, kRTWDOG_InterruptFlag);
  
  GPIO_PinWrite(BOARD_USER_LED_GPIO, BOARD_USER_LED_GPIO_PIN, 1U);*/

/* Add for ARM errata 838869, affects Cortex-M4, Cortex-M4F, Cortex-M7, Cortex-M7F Store immediate overlapping
  exception return operation might vector to incorrect interrupt */
/*#if defined __CORTEX_M && (__CORTEX_M == 4U || __CORTEX_M == 7U)
    __DSB();
#endif
}*/


/*!
 * @brief Main function.
 */
void main(void)
{
  BOARD_ConfigMPU();
  BOARD_InitPins();
  BOARD_BootClockRUN();
  //BOARD_InitDebugConsole();
  BOARD_InitModuleClock();
  // Enable RTWDOG clock
  //CLOCK_EnableClock(kCLOCK_Wdog3);
  //NVIC_EnableIRQ(RTWDOG_IRQn);
  SCB_DisableDCache();
  
  QSP_flexspi_nor_flash_init();
  
  
  
  // initialize and enable LED
  LED_INIT();
     
  TIM_Init_Class();
    
  sysStateMachineObj.state = SYS_INIT_STATE;
  
  urtStateMachineObj[0].state = URT_INIT_STATE;
  urtStateMachineObj[0].settings.interruptID = URT_LPUART1_IRQn;
  urtStateMachineObj[0].settings.port = URT_LPUART1;
  urtStateMachineObj[0].settings.baudRate = URT_DEFAULT_BAUD_RATE;
  urtStateMachineObj[0].settings.parityMode = kLPUART_ParityDisabled;
  urtStateMachineObj[0].settings.dataBitsCount = kLPUART_EightDataBits;
  urtStateMachineObj[0].settings.stopBitCount = kLPUART_OneStopBit;
  urtStateMachineObj[0].settings.configDMA = URT_DMA_CONFIG_PORT_1;
    
  urtStateMachineObj[1].state = URT_INIT_STATE;
  urtStateMachineObj[1].settings.interruptID = URT_LPUART2_IRQn;
  urtStateMachineObj[1].settings.port = URT_LPUART2;
  urtStateMachineObj[1].settings.baudRate = URT_DEFAULT_BAUD_RATE;
  urtStateMachineObj[1].settings.parityMode = kLPUART_ParityDisabled;
  urtStateMachineObj[1].settings.dataBitsCount = kLPUART_EightDataBits;
  urtStateMachineObj[1].settings.stopBitCount = kLPUART_OneStopBit;
  urtStateMachineObj[1].settings.configDMA = URT_DMA_CONFIG_PORT_2;

  
  
  
  
  
  
  //__disable_irq();
  //RTWDOG_InitModule();
  //__enable_irq();
  

  while(1)
  {  
    //RTWDOG_Refresh(RTWDOG);


    URT_StateMachine(&urtStateMachineObj[0]);
    URT_StateMachine(&urtStateMachineObj[1]);
    SYS_StateMachine();
    
    
    /*uint32_t addr = 20 * QSP_SECTOR_SIZE + 256;
    
    // Enter quad mode. 
    status = QSP_flexspi_nor_enable_quad_mode();
    if (status != kStatus_Success)
    {
        return;
    }

    // Erase sectors. 
    status = QSP_flexspi_nor_flash_erase_sector(20);
    status = QSP_flexspi_nor_flash_erase_sector(21);
    
    if (status != kStatus_Success)
    {
        return;
    }
    
    
    for (i = 0; i < 256; i++)
    {
        s_nor_program_buffer[i] = i;
    }

    status = QSP_flexspi_nor_flash_page_program(addr, (void *)s_nor_program_buffer);
    
    if (status != kStatus_Success)
    {
        return;
    }*/

    //DCACHE_CleanInvalidateByRange(QSP_FLEXSPI_AMBA_BASE + 20 * QSP_SECTOR_SIZE + 5, QSP_FLASH_PAGE_SIZE);
    
    
  } 
}

/***************************************************************************//**
* FuncName: {BOARD_InitModuleClock}
*
* @brief This function inizialize clock.
*
* @details This function inizialize clock.
*
* @todo None
*
* IN:
* @param[in] None
*            
*
* OUT:
* @param[out] None
*
* RET:
* @retval <OK> None
*
*
* @author {Luca Tollot} @date {07/11/2019}
******************************************************************************/
  
void BOARD_InitModuleClock()
{
  // Enable clock gate for GPIO1 
  CLOCK_EnableClock(kCLOCK_Gpio1);

  // Set PERCLK_CLK source to OSC_CLK
  CLOCK_SetMux(kCLOCK_PerclkMux, 1U);
  
  // Set PERCLK_CLK divider to 1 
  CLOCK_SetDiv(kCLOCK_PerclkDiv, 0U);
}

/***************************************************************************//**
* FuncName: {RTWDOG_InitModule}
*
* @brief This function inizialize rtwdog.
*
* @details This function inizialize rtwdog.
*
* @todo None
*
* IN:
* @param[in] None
*            
*
* OUT:
* @param[out] None
*
* RET:
* @retval <OK> None
*
*
* @author {Luca Tollot} @date {15/11/2019}
******************************************************************************/
  
void RTWDOG_InitModule()
{
  /*
  * config.enableWdog32 = true;
  * config.clockSource = kWDOG32_ClockSource1;
  * config.prescaler = kWDOG32_ClockPrescalerDivide1;
  * config.testMode = kWDOG32_TestModeDisabled;
  * config.enableUpdate = true;
  * config.enableInterrupt = false;
  * config.enableWindowMode = false;
  * config.windowValue = 0U;
  * config.timeoutValue = 0xFFFFU;
  */
  
  /*RTWDOG_GetDefaultConfig(&rtwdogconfig);

  rtwdogconfig.testMode = kRTWDOG_UserModeEnabled;
  rtwdogconfig.prescaler = kRTWDOG_ClockPrescalerDivide256;
  rtwdogconfig.enableInterrupt = true;
  //rtwdogconfig.timeoutValue = 300U;

  RTWDOG_Init(RTWDOG, &rtwdogconfig);*/
  
  /*
  * wdogConfig->enableWdog = true;
  * wdogConfig->workMode.enableWait = true;
  * wdogConfig->workMode.enableStop = false;
  * wdogConfig->workMode.enableDebug = false;
  * wdogConfig->enableInterrupt = false;
  * wdogConfig->enablePowerdown = false;
  * wdogConfig->resetExtension = flase;
  * wdogConfig->timeoutValue = 0xFFU;
  * wdogConfig->interruptTimeValue = 0x04u;
  */
        
  /*WDOG_GetDefaultConfig(&wdogconfig);
  wdogconfig.timeoutValue = 0x0U; // Timeout value is 8 sec. 
  //wdogconfig.enableInterrupt = true;
  //wdogconfig.interruptTimeValue = 0x4U; // Interrupt occurred 2 sec before WDOG timeout. 
  
  WDOG_Init(WDOG1, &wdogconfig);*/
  
}

/***************************************************************************//**
* FuncName: {LPI2C_UserCallback}
*
* @brief This function manages LPI2C callback.
*
* @details This function manages LPI2C callback.
*
* @todo None
*
* IN:
* @param[in] None
*            
*
* OUT:
* @param[out] None
*
* RET:
* @retval <OK> None
*
*
* @author {Luca Tollot} @date {07/11/2019}
******************************************************************************/

/*void LPI2C_UserCallback(LPI2C_Type *base, lpi2c_master_edma_handle_t *handle, status_t completionStatus, void *userData)
{
  if(base == PRM_LPI2C)
    PRM_LPI2C_UserCallback(base, handle, completionStatus, userData);
}*/

/***************************************************************************//**
* FuncName: {LPUART_UserCallback}
*
* @brief This function manages LPUART callback.
*
* @details This function manages LPUART callback.
*
* @todo None
*
* IN:
* @param[in] None
*            
*
* OUT:
* @param[out] None
*
* RET:
* @retval <OK> None
*
*
* @author {Luca Tollot} @date {11/12/2019}
******************************************************************************/

void LPUART_UserCallback(LPUART_Type *base, lpuart_edma_handle_t *handle, status_t status, void *userData)
{
  uint8_t i;

  for(i = 0;i < sizeof(urtStateMachineObj) / sizeof(urtStateMachine_t);i++)
  {
    if(base == urtStateMachineObj[i].settings.port)
    {
      URT_LPUART_UserCallback(base, handle, status, userData);
      return;
    }
  }
}



