/***************************************************************************//**
* @file IMXRT1021_boot_tim_management.c
*
* @brief Tim Service Routines.
*
* @details This file handles timer peripherals.
*
*
* @todo None
*
*
* @author Luca Tollot @date {29/12/2019}
******************************************************************************/

/* Includes ------------------------------------------------------------------*/
#include "IMXRT1021_boot_main_ram.h"




uint8_t val = 0;


/***************************************************************************//**
* FuncName: {TIM_Init_Class}
*
* @brief This function initializes tim object.
*
* @details This function initializes tim object and returns state
*          of initialization.
*
* @todo None
*
* IN:
* @param[in] None
*            
*
* OUT:
* @param[out] None
*
* RET:
* @retval <OK> 0
* @retval <NOK> -1
*
*
* @author {Luca Tollot} @date {20/09/2019}
******************************************************************************/
   
int16_t TIM_Init_Class()
{
  timControlStatusObj.timMng.millCnt = 0;
  timControlStatusObj.timMng.dec = 0;
  timControlStatusObj.timMng.sec = 0;
  timControlStatusObj.timMng.min = 0;
  timControlStatusObj.timMng.hours = 0;
    
   
  timControlStatusObj.pitConfig.enableRunInDebug = false;
  
  PIT_GetDefaultConfig(&timControlStatusObj.pitConfig);

  // Init pit module 
  PIT_Init(PIT, &timControlStatusObj.pitConfig);

  // Set timer period for channel 0 
  PIT_SetTimerPeriod(PIT, kPIT_Chnl_0, USEC_TO_COUNT(TIM_CLOCK_PERIOD, PIT_SOURCE_CLOCK));

  // Enable timer interrupts for channel 0 
  PIT_EnableInterrupts(PIT, kPIT_Chnl_0, kPIT_TimerInterruptEnable);

  // Enable at the NVIC 
  EnableIRQ(PIT_IRQ_ID);

  // Start channel 0 
  PIT_StartTimer(PIT, kPIT_Chnl_0);
  
  
  
  // Define the init structure for the output LED pin
  gpio_pin_config_t led_config = {kGPIO_DigitalOutput, 0, kGPIO_NoIntmode};
  // Init output LED GPIO. 
  GPIO_PinInit(GPIO1, 9, &led_config);
  
  return 0;
}

/***************************************************************************//**
* FuncName: {TIM_TimeModel}
*
* @brief This function manages time.
*
* @details This function manages time.
*
* @todo None
*
* IN:
* @param[in] None
*            
*
* OUT:
* @param[out] None
*
* RET:
* @retval <OK> 0
* @retval <NOK> -1
*
*
* @author {Luca Tollot} @date {20/09/2019}
******************************************************************************/

int16_t TIM_TimeModel()
{
  //uint16_t i;
  
  //Ogni millisecondo
  
  if((timControlStatusObj.timMng.millCnt + 1) == 100)
  {
    timControlStatusObj.timMng.millCnt = 0;
    
    //Ogni decimo

    if((timControlStatusObj.timMng.dec + 1) == 10)
    {
      timControlStatusObj.timMng.dec = 0;

      //Ogni secondo
                 
      LED_TOGGLE();
      
      if((timControlStatusObj.timMng.sec + 1) == 60)
      {
        timControlStatusObj.timMng.sec = 0;
        
        //Ogni minuto
                
        if((timControlStatusObj.timMng.min + 1) == 60)
        {
          timControlStatusObj.timMng.min = 0;
          
          //Ogni ora
          
          if((timControlStatusObj.timMng.hours + 1) == 24)
          {
            timControlStatusObj.timMng.hours = 0;
            
            //Ogni giorno
            
          }
          else timControlStatusObj.timMng.hours++;
        }
        else timControlStatusObj.timMng.min++;
      }
      else timControlStatusObj.timMng.sec++;
    }
    else timControlStatusObj.timMng.dec++;
    
  }
  else timControlStatusObj.timMng.millCnt++;
  
  return 0;
}

/***************************************************************************//**
* FuncName: {TIM_RTI_HANDLER}
*
* @brief This function maneges tim interrupt.
*
* @details This function manages the counters for energy meters, adjusts the 
*          sample frequency, manages the leds and manages Sync.
*
* @todo None
*
* IN:
* @param[in] None
*            
*
* OUT:
* @param[out] None
*
* RET:
* @retval <OK> None
*
*
* @author {Luca Tollot} @date {20/09/2019}
******************************************************************************/

void TIM_PIT_IRQHandler(void)
{
  uint16_t i;
  
  // Clear interrupt flag
  PIT_ClearStatusFlags(PIT, kPIT_Chnl_0, kPIT_TimerFlag);
    
  
  if(sysStateMachineObj.timeOutMachine > 0) //Timeout sys state machine
    sysStateMachineObj.timeOutMachine--;

  if(sysStateMachineObj.timeOutBootLife > 0) //Timeout boot life
    sysStateMachineObj.timeOutBootLife--;
  
  for(i = 0;i < sizeof(urtStateMachineObj) / sizeof(urtStateMachine_t);i++)
  {
    if(urtStateMachineObj[i].timeOutMachine > 0) //Timeout urt state machine
      urtStateMachineObj[i].timeOutMachine--; 

    if(urtStateMachineObj[i].state == URT_MODBUS_STATE) //Gestione modbus
    {
      if(urtStateMachineObj[i].stateUart.ONGOING == 1) //Gestione ricezione seriale
      {
        if(urtStateMachineObj[i].timeOutRx > 0)
          urtStateMachineObj[i].timeOutRx--;
		
        if(urtStateMachineObj[i].timeOutRx == 0) //Fine ricezione
        {
          urtStateMachineObj[i].stateUart.ENDRX = 1;
          urtStateMachineObj[i].stateUart.ONGOING = 0;
        }
      }
    }
  }
  

  TIM_TimeModel(); //Time base
  
  /*if(val == 1)
  {
    GPIO_PinWrite(GPIO1, 9, 1);
    val = 0;
  }
  else
  {
    GPIO_PinWrite(GPIO1, 9, 0);
    val = 1;
  }*/
}
